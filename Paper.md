# Browser Rendering
---
Introduction
---
In the process of building a good web page , we take some important factors in consideration to provide good user experience. Followings are some problems that may occur in a website.
> - Slow loading speed
> - Waiting for unnecessary files to download on initial render.
> - Flash of unstyled content
> - Etc.
 
To avoid such problems, we need to understand the lifecycle of how a browser renders a typical web page.
 
---
 
 
Working of browser
---
 
When a browser makes a request to a server to get an HTML document, an HTML page in binary stream format is received by a server . That file is a text file with the response header Content-Type set to the value text/html; charset=UTF-8. After that browser renders a beautiful-looking webpage from a simple HTML file with the help of **DOM** **CSSOM** and **RENDER TREE**.
 
DOM
---
>When a browser reads an HTML File it comes across different HTML elements e.g ```<title>, <body>```, etc. it converts every such element to a javascript object called node.
As we know HTML elements are nested inside each other and the same happens with the nodes and it makes a tree-like structure called a DOM tree .
 
 
![](https://miro.medium.com/max/650/1*YSA8lCfCVPn3d6GWAVokrA.png)
 
CSSOM
---
 
> After the creation of DOM tree browsers reads css from all sources i.e external, embedded, inline, user-agent, etc. and create CSSOM (**CSS Object Model**) which is also a tree structure just like a DOM Tree.
 
![](https://miro.medium.com/max/446/1*DJg1yRx-AzkZposWbJKcaA.png)

In CSSOM tree nodes for elements which are not going to be displayed in the browser will not be created e.g ```<title>, <head>```, etc.
 
Rendering Tree
---
 
>Render Tree is a combination of DOM tree and CSSOM tree together. When the browser has to calculate the layout of each element it refers to Render Tree, so that it can only calculate for the visible elements.
 
![](https://miro.medium.com/max/1250/1*8HnhiojSoPaJAWkruPhDwA.png)
 
Rendering Process
---
Firstly when the browser reads a web page it creates a DOM Tree , CSSOM Tree and then finally , Rendering tree. After that it starts printing elements on screen. Which are followed by the following processes.
>1. Layout Operation
>2. Painting Operation
>3. Composition Operation
 
1. ***Layout Operation***
In layout operation, the size of each node in pixels and positions are calculated from the rendering tree.
 
2. ***Paint Operation***
The painting operation layer for each element is created from the rendering tree.This layers also help the browser to correctly draw elements in the stacking order.
 
2.  ***Composition Operation***
After the Painting operation we have the different layers of elements. In compositing operations, these layers are sent to the GPU to finally draw it on the screen.
 
![](https://miro.medium.com/max/1250/1*yQJkz12sPxS-kJoMDqzbEQ.png)

